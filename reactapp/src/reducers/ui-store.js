import { createSlice } from "@reduxjs/toolkit";

export const uiStore = createSlice({
  name: "uiStore",
  initialState: {
    title: "",
  },
  reducers: {
    setTitle: (state, action) => {
      console.log(action.payload);
      state.title = action.payload;
    },
  },
});

export const { setTitle } = uiStore.actions;

export default uiStore.reducer;