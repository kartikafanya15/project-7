import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setTitle } from "../reducers/ui-store";

const Home = () => {
  const dispatch = useDispatch();
  const currentTitle = useSelector((state) => state.ui.title);
  const [customTitle, setCustomTitle] = useState(currentTitle);
  return (
    <>
      <h1>Home Page</h1>
      <input
        type="text"
        className="form-control"
        value={customTitle}
        onChange={(e) => setCustomTitle(e.target.value)}
      />
      <hr />
      <button
        className="btn btn-primary"
        onClick={() => dispatch(setTitle(customTitle))}
      >
        Change title
      </button>
    </>
  );
};

export default Home;