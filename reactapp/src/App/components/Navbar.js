const Navbar = () => {
    return (
      <nav className="navbar navbar-expand navbar-dark bg-primary">
        <a className="navbar-brand" href="/">
          App Name
        </a>
        <div className="collapse navbar-collapse">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link" href="/">
                Home
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/cars">
                Cari Mobil
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  };
  
  export default Navbar;