import { configureStore } from "@reduxjs/toolkit";

import uiStore from "../reducers/ui-store";

export default configureStore({
  reducer: {
    ui: uiStore,
  },
});